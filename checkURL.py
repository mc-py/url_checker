import requests
import os
import urllib

print ("--- LET'S START! ---")

pathToLocalFolder = (os.path.dirname(os.path.abspath(__file__)))
pathToURLs = (pathToLocalFolder+'\\URLS.txt')
pathToExtenstions = (pathToLocalFolder+'\\extenstions.txt')
urlsList=open(pathToURLs, 'r').readlines()
extensionsList=open(pathToExtenstions, 'r').readlines()

urlsWithExtList = []

for everyURL in urlsList:
    for everyExtenstion in extensionsList:
        if everyURL.endswith("\n"):
            everyURL=everyURL[:-1]
        if everyExtenstion.endswith("\n"):
            everyExtenstion=everyExtenstion[:-1]
        urlsWithExtList.append((str(everyURL)+str(everyExtenstion)))

for urlToCheck in urlsWithExtList:
    try:
        request = requests.get(urlToCheck)
        if request.status_code == 200:
            webPage = urllib.request.urlopen(urlToCheck).read().decode("utf-8")
            print(urlToCheck)
    except:
        print('')

print ("--- THE END ---")
